﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Acr.UserDialogs;
using AirChecker.Model;
using Plugin.Geolocator;

namespace AirChecker.Service
{
    public class GeolocationService
    {
        private LocationModel location;
        private bool isGeolocation;

        public GeolocationService()
        {
            createItem();
            setItem();
        }

        private void createItem()
        {
            location = new LocationModel();
            
        }

        private void setItem()
        {
            isGeolocation = false;
        }

        public async Task<bool> SetGeolocation()
        {
            if(Service.PermissionsLocationService.Instance.Plugin_isGeolocationAvailable() && Service.PermissionsLocationService.Instance.Plugin_isGeolocationEnabled())
            {
                try
                {
                    var results = await CrossGeolocator.Current.GetPositionAsync(Configuration.Configuration.Instance.timeToGetLocation);
                    if (results != null)
                    {
                        location.latitude = results.Latitude;
                        location.longitude = results.Longitude;
                        isGeolocation = true;
                        return true;
                    }
                    else
                    {
                        var secondProbe = await UserDialogs.Instance.ConfirmAsync("Wystąpił problem z pobraniem lokalizacji. Czy chcesz spróbować ponownie?", "Lokalizacja", "Tak", "Nie");
                        if (secondProbe == true)
                        {
                            results = await CrossGeolocator.Current.GetPositionAsync(Configuration.Configuration.Instance.timeToGetLocationSeconProbe);
                            if (results != null)
                            {
                                location.latitude = results.Latitude;
                                location.longitude = results.Longitude;
                                isGeolocation = true;
                                return true;
                            }
                            else
                            {
                                await UserDialogs.Instance.AlertAsync("Wystąpił problem z pobraniem lokalizacji. Spróbuj ponownie później.", "Lokalizacja", "OK");
                                isGeolocation = true;
                                return false;
                            }
                        }
                        else
                        {
                            isGeolocation = false;
                            return false;
                        }
                    }
                }
                catch(Exception ex1)
                {
                    var error1 = ex1.Message;

                    try
                    {
                        var thirdProbe = await UserDialogs.Instance.ConfirmAsync("Wystąpił problem z pobraniem lokalizacji. Czy chcesz spróbować ponownie?", "Lokalizacja", "Tak", "Nie");
                        if (thirdProbe == true)
                        {
                            var results = await CrossGeolocator.Current.GetPositionAsync(Configuration.Configuration.Instance.timeToGetLocationThirdProbe);
                            if (results != null)
                            {
                                location.latitude = results.Latitude;
                                location.longitude = results.Longitude;
                                isGeolocation = false;
                                return true;
                            }
                            else
                            {
                                await UserDialogs.Instance.AlertAsync("Wystąpił problem z pobraniem lokalizacji. Spróbuj ponownie później.", "Lokalizacja", "OK");
                                isGeolocation = false;
                                return false;
                            }
                        }
                        else
                        {
                            isGeolocation = false;
                            return false;
                        }
                    }
                    catch (Exception ex2)
                    {
                        var error2 = ex2.Message;
                        isGeolocation = false;
                        return false;
                    }
                }
            }
            else
            {
                await UserDialogs.Instance.AlertAsync("Aplikacja do działania wymaga nawigacji GPS.", "Lokalizacja", "OK");
                return false;
            }
        }
        public LocationModel GetGeolocation()
        {
            return location;
        }
        public bool IsGeolocation()
        {
            return isGeolocation;
        }
    }
}
