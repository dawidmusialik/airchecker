﻿using Acr.UserDialogs;
using Plugin.Geolocator;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AirChecker.Service
{
    public sealed class PermissionsLocationService
    {

        private static readonly object PadLock = new object();
        private static PermissionsLocationService _Instance = null;
        private bool permissionsIsGranted;

        public PermissionsLocationService()
        {
            createItem();

            Task.WaitAll(checkPermissions());
        }

        public static PermissionsLocationService Instance
        {
            get
            {
                lock (PadLock)
                {
                    if (_Instance == null)
                    {
                        _Instance = new PermissionsLocationService();
                    }
                    return _Instance;
                }
            }
        }

        private void createItem()
        {
            permissionsIsGranted = false;
        }

        private async Task checkPermissions()
        {
            try
            {
                if(CrossGeolocator.Current.IsGeolocationAvailable) //Jeżeli pozwolenie na golokalizację jest przyznane
                {
                    permissionsIsGranted = true;
                }
                else
                {
                    var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Location);
                    if (status != PermissionStatus.Granted)
                    {

                        var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Location);
                        if (results.ContainsKey(Permission.Location))
                            status = results[Permission.Location];
                    }
                    if (status == PermissionStatus.Granted)
                    {
                        permissionsIsGranted = true;
                    }
                    else if (status == PermissionStatus.Denied)
                    {
                        await UserDialogs.Instance.AlertAsync("Nie zezwolono na pobieranie lokalizacji GPS do poprawnego działani aplikacji jest ona konieczna.", "Lokalizacja", "OK");
                        permissionsIsGranted = false;
                    }
                    else if (status != PermissionStatus.Granted && status != PermissionStatus.Denied)
                    {
                        await UserDialogs.Instance.AlertAsync("Aplikacja do poprawnego działania wymaga zezwolenia na pobranie lokalizacji.", "Lokalizacja", "OK");
                        permissionsIsGranted = false;
                    }
                }
            }
            catch(Exception ex)
            {
                var error = ex.Message;
            }
        }

        public bool Plugin_isGeolocationEnabled()
        {
            return CrossGeolocator.Current.IsGeolocationEnabled;
        }
        public bool Plugin_isGeolocationAvailable()
        {
            return CrossGeolocator.Current.IsGeolocationAvailable;
        }
        public bool Code_isGeolocationAvailable()
        {
            return permissionsIsGranted;
        }
        public async void CheckGeolocationPermissions()
        {
            await checkPermissions();
        }
    }
}
