﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AirChecker.Configuration
{
    public sealed class ConfigurationApi
    {
        private static readonly object PadLock = new object();
        private static ConfigurationApi _Instance = null;

        private string httpsAdress;
        private string apiKey;

        private string _apiInstallationsNearest;
        private string _apiInstallationId;
        private string _apiMeasurementsInstallationId;

        private ConfigurationApi()
        {
            createAndSetItem();
        }
        private void createAndSetItem()
        {

            httpsAdress = "https://airapi.airly.eu/v2/";
            apiKey = "tj1BevRRRxaoUZauJxBP87lEXRZxhYg3";
        }

        public static ConfigurationApi Instance
        {
            get
            {
                lock (PadLock)
                {
                    if (_Instance == null)
                    {
                        _Instance = new ConfigurationApi();
                    }
                    return _Instance;
                }
            }
        }

        public string getApiKey()
        {
            return apiKey;
        }

        public string apiInstallationsNearest(double latitude, double longitude)
        {
            _apiInstallationsNearest = httpsAdress + "installations/nearest";
            _apiInstallationsNearest += "?lat=" + DisplayNDecimal(latitude, 6);
            _apiInstallationsNearest += "&lng=" + DisplayNDecimal(longitude, 6);
            _apiInstallationsNearest += "&maxDistanceKM=3&maxResults=1";
            //_apiInstallationsNearest += "&apikey=" + apiKey;

            StringBuilder _stringBuilder = new StringBuilder(_apiInstallationsNearest);
            _stringBuilder.Replace(",", ".");

            return _stringBuilder.ToString();
        }
        private string DisplayNDecimal(double dbValue, int nDecimal)
        {
            string decimalPoints = "0";
            if (nDecimal > 0)
            {
                decimalPoints += ".";
                for (int i = 0; i < nDecimal; i++)
                    decimalPoints += "0";
            }
            return dbValue.ToString(decimalPoints);
        }
        public string apiInstallationId(int instalationId)
        {
            _apiInstallationId = httpsAdress + "installations/";
            _apiInstallationId += instalationId.ToString();

            return _apiInstallationId;
        }
        public string apiMeasurementsInstallationId(int instalationId)
        {
            _apiMeasurementsInstallationId = httpsAdress + "measurements/installation/?";
            _apiMeasurementsInstallationId += "indexType=AIRLY_CAQI";
            _apiMeasurementsInstallationId += "&installationId=" + instalationId.ToString();

            return _apiMeasurementsInstallationId;
        }
    }
}

