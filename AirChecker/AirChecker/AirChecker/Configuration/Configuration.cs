﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AirChecker.Configuration
{
    public sealed class Configuration
    {
        private static readonly object PadLock = new object();
        private static Configuration _Instance = null;

        public TimeSpan timeToGetLocation { get; set; }
        public TimeSpan timeToGetLocationSeconProbe { get; set; }
        public TimeSpan timeToGetLocationThirdProbe { get; set; }


        private Configuration()
        {
            timeToGetLocation = new TimeSpan(0, 0, 15);
            timeToGetLocationSeconProbe = new TimeSpan(0, 0, 30);
            timeToGetLocationThirdProbe = new TimeSpan(0, 1, 0);

        }


        public static Configuration Instance
        {
            get
            {
                lock (PadLock)
                {
                    if (_Instance == null)
                    {
                        _Instance = new Configuration();
                    }
                    return _Instance;
                }
            }
        }
    }
}
