﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using AirChecker.ViewModel;
using System.Threading.Tasks;

namespace AirChecker.View
{
    public class AirCheckerView:ContentPage
    {
        private AirCheckerViewModel viewItem;
        private StackLayout stackLayout;
        private ScrollView scrollView;

        public AirCheckerView()
        {
            createItem();
            setItem();
        }

        private void createItem()
        {
            viewItem = new AirCheckerViewModel();
            stackLayout = new StackLayout();
            scrollView = new ScrollView();
        }

        private void setItem()
        {
            stackLayout.Children.Add(viewItem.appNameLabel);
            stackLayout.Children.Add(viewItem_AirAndRefresh());

            stackLayout.Children.Add(viewItem.refreshButton);

            scrollView.Margin = new Thickness(10, 5, 10, 5);
            scrollView.Content = stackLayout;

            this.Title = "AirChecker";
            this.Content = scrollView;
            
        }

        private Grid viewItem_AirAndRefresh()
        {
            Grid _tmpGrid = new Grid()
            {
                ColumnDefinitions =
                {
                    new ColumnDefinition(){ Width = new GridLength(1, GridUnitType.Auto) },
                    new ColumnDefinition(){ Width = new GridLength(1, GridUnitType.Auto) }
                },
                RowDefinitions =
                {
                    new RowDefinition() {Height = new GridLength(1, GridUnitType.Auto)},
                    new RowDefinition() {Height = new GridLength(1, GridUnitType.Auto)}
                },
            };

            _tmpGrid.Children.Add(viewItem.airLabel, 0, 1, 0, 1);
            _tmpGrid.Children.Add(viewItem.airDetailLabel, 1, 2, 0, 1);
            _tmpGrid.Children.Add(viewItem.refreshLabel, 0, 1, 1, 2);
            _tmpGrid.Children.Add(viewItem.refreshDetailLabel, 1, 2, 1, 2);

            _tmpGrid.HorizontalOptions = LayoutOptions.CenterAndExpand;
            _tmpGrid.VerticalOptions = LayoutOptions.CenterAndExpand;

            return _tmpGrid;
        }
       
        
    }
}
