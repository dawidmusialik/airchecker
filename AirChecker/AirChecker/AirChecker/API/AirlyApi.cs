﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using AirChecker.Model.Measurements;
using AirChecker.Model.Installation;
using Newtonsoft.Json;
using AirChecker.Model;

namespace AirChecker.API
{
    public sealed class  AirlyApi
    {
        private static readonly object PadLock = new object();
        private static AirlyApi _Instance = null;

        public AirlyApi()
        {

        }

        public static AirlyApi Instance
        {
            get
            {
                lock (PadLock)
                {
                    if (_Instance == null)
                    {
                        _Instance = new AirlyApi();
                    }
                    return _Instance;
                }
            }
        }
        public async Task<InstallationNearestModel> apiInstallationsNearest(LocationModel _location)
        {
            HttpClient _client = new HttpClient();

            //_client.DefaultRequestHeaders.Clear();
            _client.DefaultRequestHeaders.Add("Accept","application/json");
            _client.DefaultRequestHeaders.Add("Accept-Language", "pl");
            _client.DefaultRequestHeaders.Add("apikey", Configuration.ConfigurationApi.Instance.getApiKey());

            string URL = Configuration.ConfigurationApi.Instance.apiInstallationsNearest(_location.latitude, _location.longitude);

            try
            {
                var response = await _client.GetAsync(URL);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var t = await response.Content.ReadAsStringAsync();
                    var temp = JsonConvert.DeserializeObject<List<InstallationNearestModel>>(t);
                    return temp.FirstOrDefault();
                }
                else
                {
                    return null;
                }
            }
            catch(Exception ex)
            {
                var error = ex.Message;
                return null;
            }
        }
        public async Task<InstalationIdModel> apiInstallationId(int _installationId)
        {
            HttpClient _client = new HttpClient();

            //_client.DefaultRequestHeaders.Clear();
            _client.DefaultRequestHeaders.Add("Accept", "application/json");
            _client.DefaultRequestHeaders.Add("Accept-Language", "pl");
            _client.DefaultRequestHeaders.Add("apikey", Configuration.ConfigurationApi.Instance.getApiKey());

            string URL = Configuration.ConfigurationApi.Instance.apiInstallationId(_installationId);

            try
            {
                var response = await _client.GetAsync(URL);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var t = await response.Content.ReadAsStringAsync();
                    var temp = JsonConvert.DeserializeObject<InstalationIdModel>(t);
                    return temp;
                }
                else if(response.StatusCode == HttpStatusCode.MovedPermanently)
                {
                    return null;
                }
                else if(response.StatusCode == HttpStatusCode.NotFound)
                {
                    return null;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                var error = ex.Message;
                return null;
            }
        }
        public async Task<MeasurementsInstallationIdModel> apiMeasurementsInstallationId(int _installationId)
        {
            HttpClient _client = new HttpClient();

            //_client.DefaultRequestHeaders.Clear();
            _client.DefaultRequestHeaders.Add("Accept", "application/json");
            _client.DefaultRequestHeaders.Add("Accept-Language", "pl");
            _client.DefaultRequestHeaders.Add("apikey", Configuration.ConfigurationApi.Instance.getApiKey());

            string URL = Configuration.ConfigurationApi.Instance.apiMeasurementsInstallationId(_installationId);

            try
            {
                var response = await _client.GetAsync(URL);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var t = await response.Content.ReadAsStringAsync();
                    var temp = JsonConvert.DeserializeObject<MeasurementsInstallationIdModel>(t);
                    return temp;
                }
                else if (response.StatusCode == HttpStatusCode.MovedPermanently)
                {
                    return null;
                }
                else if (response.StatusCode == HttpStatusCode.NotFound)
                {
                    return null;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                var error = ex.Message;
                return null;
            }
        }
    }
}
