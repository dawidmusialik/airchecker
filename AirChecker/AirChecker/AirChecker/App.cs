﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using AirChecker.View;
using AirChecker.Service;
using System.Linq;
using System.Threading.Tasks;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace AirChecker
{
    public partial class App : Application
    {
        public App()
        {
            MainPage = new NavigationPage(new AirCheckerView());


            Console.WriteLine("Geolokalizacja dozwolona? - " + PermissionsLocationService.Instance.Code_isGeolocationAvailable().ToString());
            
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
