﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using AirChecker.Service;
using System.Threading.Tasks;
using Acr.UserDialogs;
using System.Linq;
using Plugin.Connectivity;

namespace AirChecker.ViewModel
{
    public class AirCheckerViewModel
    {
        public Label appNameLabel { get; set; }
        public Label airLabel { get; private set; } 
        public Label airDetailLabel { get; private set; }

        public Label refreshLabel { get; private set; }
        public Label refreshDetailLabel { get; private set; }
        public Button refreshButton { get; private set; }

        private GeolocationService geolocationService;

        private bool refreshButton_IsClicked;

        public AirCheckerViewModel()
        {
            createItem();
            setItem();
            setItem_PlaceOnScreen();
        }

        private void createItem()
        {
            appNameLabel = createLabel();
            airLabel = createLabel();
            airDetailLabel = createLabel();
            refreshLabel = createLabel();
            refreshDetailLabel = createLabel();
            refreshButton = createButton();

            geolocationService = new GeolocationService();
        }
        private void setItem()
        {
            appNameLabel.Text = "AirChecker";
            appNameLabel.FontSize = 20;
            appNameLabel.FontAttributes = FontAttributes.Bold;
            appNameLabel.VerticalOptions = LayoutOptions.Start;
            appNameLabel.HorizontalOptions = LayoutOptions.CenterAndExpand;

            airLabel.Text = "Dzisiejsze powietrze: ";
            airDetailLabel.HorizontalTextAlignment = TextAlignment.Center;

            refreshDetailLabel.HorizontalTextAlignment = TextAlignment.Center;
            refreshLabel.Text = "Odświeżone: ";

            refreshButton.Text = "Odśwież";
            refreshButton_IsClicked = false;
            refreshButton.Clicked += async (sender, e) => await getDataAndSetOnView(sender, e);;
        }

        private void setItem_PlaceOnScreen()
        {
            refreshButton.HorizontalOptions = LayoutOptions.Center;
        }


    private async Task getDataAndSetOnView(object sender, EventArgs e)
    {
        if (refreshButton_IsClicked == false)
        {
            refreshButton_IsClicked = true;
            try
            {
                await geolocationService.SetGeolocation();

                if (geolocationService.IsGeolocation() == true)
                {
                    if (CrossConnectivity.Current.IsConnected == true)
                    {
                        var installationNearest = await API.AirlyApi.Instance.apiInstallationsNearest(geolocationService.GetGeolocation());

                        if (installationNearest != null)
                        {
                            var measurementsInstallationId = await API.AirlyApi.Instance.apiMeasurementsInstallationId(installationNearest.id);
                            if (measurementsInstallationId != null)
                            {
                                UserDialogs.Instance.Toast("Latitude: " + geolocationService.GetGeolocation().latitude + "\n" + "Longitude: " + geolocationService.GetGeolocation().longitude);
                                Device.BeginInvokeOnMainThread(() =>
                                {
                                    airDetailLabel.Text = measurementsInstallationId.current.indexes.FirstOrDefault().description + "\n";
                                    airDetailLabel.Text += measurementsInstallationId.current.indexes.FirstOrDefault().advice;
                                    refreshDetailLabel.Text = DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString();
                                });
                            }
                        }
                    }
                    else
                    {
                        await UserDialogs.Instance.AlertAsync("Do pobrania danych potrzebne jest połączenie z internetem", "Internet", "OK");
                    }
                }
                else
                {
                    await geolocationService.SetGeolocation();
                }
            }

            catch (Exception ex)
            {
                var error = ex.Message;
            }
            finally
            {
                refreshButton_IsClicked = false;
            }
        }
    }
        
       
        private Label createLabel()
        {
            Label _tmpLabel = new Label();
            _tmpLabel.FontSize = 14;
            _tmpLabel.TextColor = Color.Black;
            _tmpLabel.Text = "";
            _tmpLabel.VerticalTextAlignment = TextAlignment.Center;
            return _tmpLabel;
        }

        private Button createButton()
        {
            Button _tmpButton = new Button();
            _tmpButton.FontSize = 13;
            
            return _tmpButton;
        }
    }
}
