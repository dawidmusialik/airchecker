﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace AirChecker.Model.Installation
{
    public class Location
    {
        [JsonProperty("latitude")]
        public double latitude { get; set; }
        [JsonProperty("longitude")]
        public double longitude { get; set; }
    }

    public class Address
    {
        [JsonProperty("country")]
        public string country { get; set; }
        [JsonProperty("city")]
        public string city { get; set; }
        [JsonProperty("street")]
        public string street { get; set; }
        [JsonProperty("number")]
        public string number { get; set; }
        [JsonProperty("displayAddress1")]
        public string displayAddress1 { get; set; }
        [JsonProperty("displayAddress2")]
        public string displayAddress2 { get; set; }
    }

    public class Sponsor
    {
        [JsonProperty("name")]
        public string name { get; set; }
        [JsonProperty("description")]
        public string description { get; set; }
        [JsonProperty("logo")]
        public string logo { get; set; }
        [JsonProperty("link")]
        public object link { get; set; }
    }

    public class InstallationNearestModel
    {
        [JsonProperty("id")]
        public int id { get; set; }
        [JsonProperty("location")]
        public Location location { get; set; }
        [JsonProperty("address")]
        public Address address { get; set; }
        [JsonProperty("elevation")]
        public double elevation { get; set; }
        [JsonProperty("airly")]
        public bool airly { get; set; }
        [JsonProperty("sponsor")]
        public Sponsor sponsor { get; set; }
    }
}
