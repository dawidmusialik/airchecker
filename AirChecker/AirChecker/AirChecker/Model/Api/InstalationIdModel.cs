﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AirChecker.Model.Installation
{
    public class Value
    {
        public string name { get; set; }
        public int value { get; set; }
    }

    public class Index
    {
        public string name { get; set; }
        public int value { get; set; }
        public string level { get; set; }
        public string description { get; set; }
        public string advice { get; set; }
        public string color { get; set; }
    }

    public class Standard
    {
        public string name { get; set; }
        public string pollutant { get; set; }
        public int limit { get; set; }
        public int percent { get; set; }
    }

    public class Current
    {
        public string fromDateTime { get; set; }
        public string tillDateTime { get; set; }
        public List<Value> values { get; set; }
        public List<Index> indexes { get; set; }
        public List<Standard> standards { get; set; }
    }

    public class Value2
    {
        public string name { get; set; }
        public int value { get; set; }
    }

    public class Index2
    {
        public string name { get; set; }
        public int value { get; set; }
        public string level { get; set; }
        public string description { get; set; }
        public string advice { get; set; }
        public string color { get; set; }
    }

    public class Standard2
    {
        public string name { get; set; }
        public string pollutant { get; set; }
        public int limit { get; set; }
        public int percent { get; set; }
    }

    public class History
    {
        public string fromDateTime { get; set; }
        public string tillDateTime { get; set; }
        public List<Value2> values { get; set; }
        public List<Index2> indexes { get; set; }
        public List<Standard2> standards { get; set; }
    }

    public class Value3
    {
        public string name { get; set; }
        public int value { get; set; }
    }

    public class Index3
    {
        public string name { get; set; }
        public int value { get; set; }
        public string level { get; set; }
        public string description { get; set; }
        public string advice { get; set; }
        public string color { get; set; }
    }

    public class Standard3
    {
        public string name { get; set; }
        public string pollutant { get; set; }
        public int limit { get; set; }
        public int percent { get; set; }
    }

    public class Forecast
    {
        public string fromDateTime { get; set; }
        public string tillDateTime { get; set; }
        public List<Value3> values { get; set; }
        public List<Index3> indexes { get; set; }
        public List<Standard3> standards { get; set; }
    }

    public class InstalationIdModel
    {
        public Current current { get; set; }
        public List<History> history { get; set; }
        public List<Forecast> forecast { get; set; }
    }
}
