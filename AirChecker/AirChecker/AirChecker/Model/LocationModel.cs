﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AirChecker.Model
{
    public class LocationModel
    {
        public double longitude { get; set; } // Długość geograficzna
        public double latitude { get; set; } //Szerokość grograficzna

        public LocationModel()
        {
            longitude = 0;
            latitude = 0;
        }
    }
}
